import Vue from "vue";
import Vuex from "vuex";
import jsondata from "../assets/contacts.json";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    contacts: [],
    filterText: "",
    filterFirstLetter: ""
  },
  mutations: {
    UPDATE_CONTACTS(state, payload) {
      state.contacts = payload;
    },
    UPDATE_FILTERTEXT(state, searchtext) {
      state.filterText = searchtext;
    },
    UPDATE_FILTER_FIRST_TEXT(state, searchletter) {
      state.filterFirstLetter = searchletter;
    },
    DELETE_CONTACT(state, id) {
      let index = state.contacts.findIndex(element => element.id == id);
      state.contacts.splice(index, 1);
    },
    UPDATE_CONTACT(state, contact) {
      state.contacts = [
        ...state.contacts.filter(element => element.id != contact.id),
        contact
      ];
    },
    ADD_CONTACT(state, contact) {
      let newId = Math.max(...state.contacts.map(element => element.id));
      contact.id = newId + 1;
      state.contacts.push(contact);
    },
    CLEAR_FILTERS(state) {
      state.filterText = "";
      state.filterFirstLetter = "";
    }
  },
  actions: {
    /**
     * Executes the load of the data on first page load
     */
    initialLoad({ commit }) {
      commit("UPDATE_CONTACTS", jsondata.contacts);
    }
  },
  getters: {
    getContacts: state => state.contacts,
    getFilterText: state => state.filterText,
    getFilterFirstLetter: state => state.filterFirstLetter,

    /**
     * Returns the alphabetically sorted and filtered contacts
     * based on the filterText and firstLetterFilter state
     */
    getFilteredContacts: state => {
      let textFiltered = state.contacts.filter(data =>
        data.name.toLowerCase().includes(state.filterText.toLowerCase())
      );
      if (state.filterFirstLetter != "") {
        return textFiltered
          .filter(
            data =>
              data.name[0].toLowerCase() ==
              state.filterFirstLetter.toLowerCase()
          )
          .sort((a, b) => {
            var textA = a.name.toUpperCase();
            var textB = b.name.toUpperCase();
            return textA < textB ? -1 : textA > textB ? 1 : 0;
          });
      } else {
        return textFiltered.sort((a, b) => {
          var textA = a.name.toUpperCase();
          var textB = b.name.toUpperCase();
          return textA < textB ? -1 : textA > textB ? 1 : 0;
        });
      }
    },

    /**
     * Returns a distinct list of the first letters of each name
     * with capital letter and sorted alphabetically
     */
    getNameFirstLetters: state => {
      let textFiltered = state.contacts.filter(data =>
        data.name.toLowerCase().includes(state.filterText.toLowerCase())
      );
      let firstLetters = textFiltered.map(data => data.name[0].toUpperCase());
      let uniqueFirstLetters = [...new Set(firstLetters)];
      return uniqueFirstLetters.sort();
    },

    /**
     * Returns the contact with the provided id
     * @param {int} id - contact's unique id
     * @returns {object}
     */
    getContactById: state => id => {
      return state.contacts.find(contact => contact.id == id);
    }
  }
});
