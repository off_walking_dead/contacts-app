import Vue from "vue";
import VueRouter from "vue-router";
import Contacts from "../views/Contacts.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Contacts",
    component: Contacts,
    beforeEnter: (to, from, next) => {
      if ("user" in to.query) {
        next({
          name: "ViewContact",
          params: { contactId: Number(to.query["user"]) }
        });
      } else {
        next();
      }
    }
  },
  {
    path: "/new",
    name: "NewContact",
    component: () =>
      import(/* webpackChunkName: "newcontact" */ "../views/NewContact.vue")
  },
  {
    path: "/:contactId",
    name: "ViewContact",
    props: true,
    component: () =>
      import(/* webpackChunkName: "contact" */ "../views/Contact.vue")
  },
  {
    path: "/:contactId/:contactAction",
    name: "EditContact",
    props: true,
    component: () =>
      import(/* webpackChunkName: "contact" */ "../views/Contact.vue")
  },
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  routes
});

export default router;
