import { shallowMount } from "@vue/test-utils";
import Card from "@/components/Card.vue";

const contact = {
  name: "Test Name",
  email: "test@email.com",
  phone: "+123456789"
};

describe("Card.vue", () => {
  it("renders props.contact when passed", () => {
    
    const wrapper = shallowMount(Card, {
      propsData: { contact }
    });
    expect(wrapper.text()).toMatch(contact.name);
    expect(wrapper.text()).toMatch(contact.email);
    expect(wrapper.text()).toMatch(contact.phone);
  });

  it('has all card class elements', () => {
    const wrapper = shallowMount(Card, {
      propsData: { contact }
    });
    expect(wrapper.html()).toContain('card ');
    expect(wrapper.html()).toContain('card-header');
    expect(wrapper.html()).toContain('card-body');
  });
});